<?php

namespace App\Http\Controllers;

use App\Concert;
use Illuminate\Http\Request;

class ConcertController extends Controller
{
    public function show($id) {
        $concert = Concert::find($id);
        return view('concerts.show')->with(['concert' => $concert]);
    }
}
