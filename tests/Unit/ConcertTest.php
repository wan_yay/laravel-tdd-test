<?php


namespace Tests\Unit;


use App\Concert;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ConcertTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function can_get_formatted_date()
    {
        $concret = factory(Concert::class)->create([
            'date' => Carbon::parse('2020-12-01 8:00pm')
        ]);

        $date = $concret->formatted_date;

        $this->assertEquals('December 1, 2020', $date);
    }

    /** @test */
    function can_get_formatted_start_time()
    {
        $concret = factory(Concert::class)->create([
            'date' => Carbon::parse('2020-12-01 17:00:00')
        ]);

        $date = $concret->formatted_start_time;

        $this->assertEquals('5:00pm', $date);
    }
}
