<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Concert;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Concert::class, function (Faker $faker) {
    return [
        "title" => "The Fake Band",
        "subtitle" => 'with Fake and Lethargy',
        "date" => Carbon::parse('+2 weeks'),
        "ticket_price" => 3250,
        "venue" => "The Example Pit",
        "venue_address" => "123 Example Lane",
        "city" => "Laraville",
        "state" => "ON",
        "zip" => "17916",
        "additional_information" => "For tickets, call (555) 555-5555."
    ];
});
